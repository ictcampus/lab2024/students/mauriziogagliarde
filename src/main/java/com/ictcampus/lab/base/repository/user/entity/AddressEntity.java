package com.ictcampus.lab.base.repository.user.entity;

import com.ictcampus.lab.base.repository.book.entity.BookEntity;
import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "addresses")
public class AddressEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String street;

    @Column(nullable = false)
    private String cap;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String province;
/*
    @ManyToMany(mappedBy = "addresses") @ToString.Exclude
    List<UserEntity> users = new ArrayList<>();
    */
}
