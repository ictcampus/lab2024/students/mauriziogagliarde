package com.ictcampus.lab.base.service.book.mapper;

import com.ictcampus.lab.base.control.book.model.BookResponse;
import com.ictcampus.lab.base.repository.book.entity.BookEntity;
import com.ictcampus.lab.base.service.book.model.Book;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;



/**
 * Mapper per la conversione tra entità di servizio e DTO.
 */
@Mapper
public interface BookServiceStructMapper {
	List<Book> toBooks(List<BookEntity> bookEntities);
	Book toBook(BookEntity bookEntity);
	BookEntity toBookEntity(Book book);
	BookResponse toBookResponse(BookEntity bookEntity); //Metodo per la conversione da BookEntity a BookResponse
}

