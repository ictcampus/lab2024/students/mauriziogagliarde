package com.ictcampus.lab.base.service.user;

public enum UserRole {
    OPERATOR,
    CUSTOMER
}