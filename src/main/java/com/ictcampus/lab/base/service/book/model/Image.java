package com.ictcampus.lab.base.service.book.model;

import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
public class Image {
	private Long id;
	private String title;
	private String url;
	private boolean thumbnail;
	private List<Book> books = new ArrayList<>(); // Relazione molti a molti con Book


	/*
	public void addBook(Book book) {
		if (!this.books.contains(book)) {
			this.books.add(book);
			book.getImages().add(this);  // Aggiunge l'immagine al libro mantenendo la bidirezionalità
		}
	}

	public void removeBook(Book book) {
		if (this.books.contains(book)) {
			this.books.remove(book);
			book.getImages().remove(this);  // Rimuove l'immagine dal libro mantenendo la bidirezionalità
		}
	}
	 */
}